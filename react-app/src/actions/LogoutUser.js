import { ActionTypes } from "./ActionTypes"

const LogoutUser = () =>{
    return{
        type: ActionTypes.Logout,
        isLoggedIn: false,
        login: '',
        password: ''
    }
}

export default LogoutUser;