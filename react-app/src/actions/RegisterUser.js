import { ActionTypes } from "./ActionTypes"


const RegisterUser = (login, password, isLoggedIn) =>{
    return{
        type: ActionTypes.Register,
        isLoggedIn: isLoggedIn,
        login: login,
        password: password
    }
}

export default RegisterUser