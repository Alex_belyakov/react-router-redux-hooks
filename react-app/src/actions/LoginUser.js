import { ActionTypes } from "./ActionTypes"

const LoginUser = (login, password, isLoggedIn) =>{
    return{
        type: ActionTypes.Login,
        isLoggedIn: isLoggedIn,
        login: login,
        password: password
    }
}

export default LoginUser;