import {createStore} from 'redux'
import UserReducer from '../reducers/UserReducer'

export const userStore = createStore(UserReducer)