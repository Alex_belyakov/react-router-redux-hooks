import { ActionTypes } from "../actions/ActionTypes";


const initialState = {
    isLoggedIn: false,
    login: '',
    password: ''
}

const UserReducer = (state = initialState, action) => {
    switch(action.type) {
        case ActionTypes.Login:
            state.isLoggedIn = action.isLoggedIn;
            state.login = action.login;
            state.password = action.password;
            return {...state};
        case ActionTypes.Logout:
            state.isLoggedIn = action.isLoggedIn;
            state.login = action.login;
            state.password = action.password;
            return {...state};
        case ActionTypes.Register:
            state.isLoggedIn = action.isLoggedIn;
            state.login = action.login;
            state.password = action.password;
            return {...state};
        default:            
            state.isLoggedIn = false;
            state.login = '';
            state.password = '';
            return {...state};
    }
}

export default UserReducer