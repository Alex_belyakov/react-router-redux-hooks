import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

const Login = (props) =>
{
    const [message, setMessage] = useState({success: '', error: ''})
    const [userLogin, setLogin] = useState(props.userLogin)
    const [userPassword, setPassword] = useState('')

    let isLoggedIn = props.isLoggedIn; 
    const loginUser = props.login;
    const logoutUser = props.logout;

    
    const login = async () => {
        try{
            clearStates();

            var isValid = validateForm();
            if (!isValid){
                return;
            }

            var loginResult = await loginUser(userLogin, userPassword)

            if (loginResult.code === 0){
                setMessage({success: loginResult.message, error: ''});
            }
            else{
                setMessage({success: '', error: loginResult.message});
            }
        }
        catch(e){
            console.log(e);
            showError("Error occured when logging");
        }
    }

    const logout = async () => {
        try{
            clearStates();

            var logoutResult = await logoutUser(userLogin, userPassword)

            if (logoutResult.code === 0){
                setMessage({success: logoutResult.message, error: ''});
            }
            else{                
                setMessage({success: '', error: logoutResult.message});
            }
        }
        catch(e){
            console.log(e);
            showError("Error occured when logging out");
        }
    }    

    const handleLoginChange = (event) => {        
        setLogin(event.target.value);
    }

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    }

    const loggedInView = (
        <div className="container">
            <div className="alert alert-danger" role="alert" hidden={!message.error}>{message.error}</div>
            <div className="alert alert-primary" role="alert" hidden={!message.success}>{message.success}</div>
            
            <div className="mb-3">
                <h1>Logged in as {userLogin}</h1>
            </div>
            <div className="mb-3">
                <button className="btn btn-primary" onClick={logout.bind(this)}>LogOut</button>
            </div>
        </div>
    );

    const loggedOutView = (
        <div className="container">
            <div className="alert alert-danger" role="alert" hidden={!message.error}>{message.error}</div>
            <div className="alert alert-primary" role="alert" hidden={!message.success}>{message.success}</div>
            
            <div className="mb-3">
                <h1>Authorization Form</h1>
                <div className="mb-3">
                        <label className="form-label">Login</label>
                        <input  className="form-control" onChange={handleLoginChange.bind(this)} value={userLogin}></input>
                </div>
                <div className="mb-3">
                    <label className="form-label">Password</label>
                    <input className="form-control" onChange={handlePasswordChange.bind(this)} value={userPassword}></input>
                </div>
            </div>
            <div className="mb-3">
                <button className="btn btn-primary" onClick={login.bind(this)}>Login</button>
            </div>
        </div>
    );
    

    const clearStates = () => {
        setMessage({success: '', error: ''});
    }

    const validateForm = () => {
        if (!userLogin){
            showError("Login is empty");
            
            return false;
        }

        if (!userPassword){
            showError("Password is empty");
            
            return false;
        }

        return true;
    }

    const showError = (msg) => {
        setMessage({success: '', error: msg});
    }

    if (isLoggedIn){
        return loggedInView;
    }
    else{
        return loggedOutView;
    }
}

export default Login