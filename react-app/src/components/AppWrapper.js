import { Provider } from "react-redux"
import { Route } from "react-router-dom"
import App from "./App"
import { userStore } from "../store/userStore"
import About from "./About"
import LoginContainer from "../containers/LoginContainer"
import RegisterContainer from "../containers/RegisterContainer"

const AppWrapper = () =>{
    return (
        <Provider store = { userStore }>   
            <App>    
                <Route path="/login" component={LoginContainer} />
                <Route path="/register" component={RegisterContainer} />
                <Route exact path="/" component={About} />
            </App>
        </Provider>
    )
}

export default AppWrapper