import './App.css';
import {
  BrowserRouter as Router,
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from './NavBar';

const  App = (props) => {
  return (
    <div className="container">
      <div className="text-center align-text-middle">
      <h1>User student app</h1>
    </div>
    <Router>
      <div>
        <NavBar />
        { props.children }
      </div>
    </Router>
    </div>
  );
}

export default App