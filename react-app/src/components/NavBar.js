import { Link } from "react-router-dom"

const NavBar = () => {
    return (
        <ul  className="nav nav-tabs">
          <li className="nav-item nav-link">
            <Link className="nav-link" to="/login">Login</Link>
          </li>
          <li className="nav-item nav-link">
            <Link className="nav-link" to="/register">Register</Link>
          </li>
          <li className="nav-item nav-link">
            <Link className="nav-link" to="/">About</Link>
          </li>
        </ul>
    )
}

export default NavBar