import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

const Register = (props) =>
{
    const [message, setMessage] = useState({success: '', error: ''})
    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')

    let registerUser = props.register; 

    const handleLoginChange = (event) => {        
        setLogin(event.target.value);
    }

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    }
    
    const register = async () => {
        try{
            clearStates();

            var isValid = validateForm();
            if (!isValid){
                return;
            }

            var loginResult = await registerUser(login, password)

            if (loginResult.code === 0){
                setMessage({success: loginResult.message, error: ''});
            }
            else{
                setMessage({success: '', error: loginResult.message});
            }
        }
        catch(e){
            console.log(e);
            showError("Error occured when registering");
        }
    }

    const clearStates = () => {
        setMessage({success: '', error: ''});
    }

    const validateForm = () => {
        if (!login){
            showError("Login is empty");
            
            return false;
        }

        if (!password){
            showError("Password is empty");
            
            return false;
        }

        return true;
    }

    const showError = (msg) => {
        setMessage({success: '', error: msg});
    }

    
    return (
        <div className="container">
            <div className="alert alert-danger" role="alert" hidden={!message.error}>{message.error}</div>
            <div className="alert alert-primary" role="alert" hidden={!message.success}>{message.success}</div>
            
            <div className="mb-3">
                <h1>Registration Form</h1>
                <div className="mb-3">
                        <label className="form-label">Login</label>
                        <input  className="form-control" onChange={handleLoginChange.bind(this)}></input>
                </div>
                <div className="mb-3">
                    <label className="form-label">Password</label>
                    <input className="form-control" onChange={handlePasswordChange.bind(this)}></input>
                </div>
            </div>
            <div className="mb-3">
                <button className="btn btn-primary" onClick={register.bind(this)}>Register</button>
            </div>
        </div>
    );
}

export default Register