import axios from 'axios'
import { connect } from 'react-redux';
import RegisterUser from '../actions/RegisterUser';
import Register from '../components/Register';

const mapDispatchToProps = (dispatch) => {
    return {
        register: async (login, password) => {                
            var response = await register(login, password);
            var isLoggedIn = response.code === 0;
            dispatch(RegisterUser(login, password, isLoggedIn));
            
            return {
                code: response.code,
                message: response.message
            }
        }
    }
}

const register = async (login, password) => {
    var response = await axios.post("/api/account/register",
        {
            'login': login,
            'password': password
        }
    );

    return {
        code: response.data.code,
        message: response.data.message
    }
}

const RegisterContainer = connect(null, mapDispatchToProps)(Register);

export default RegisterContainer
