import axios from 'axios'
import { connect } from 'react-redux';
import LoginUser from '../actions/LoginUser';
import LogoutUser from '../actions/LogoutUser';
import Login from '../components/Login';

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.isLoggedIn,
        userLogin: state.login
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        login: async (login, password) => {                
            var response = await loginUser(login, password);
            var isLoggedIn = response.code === 0;
            dispatch(LoginUser(login, password, isLoggedIn));

            return {
                code: response.code,
                message: response.message
            }
        },
        logout: async (login, password) => {                
            var response = await logout(login, password);
            var isLoggedIn = response.code === 1;
            if (!isLoggedIn){
                dispatch(LogoutUser())
            }
            
            return {
                code: response.code,
                message: response.message
            }
        }
    };
}

const loginUser = async (login, password) => {
    var response = await axios.post("/api/account/login",
        {
            'login': login,
            'password': password
        }
    );

    return {
        code: response.data.code,
        message: response.data.message
    }
}

const logout = async (login, password) => {
    var response = await axios.post("/api/account/logout",
        {
            'login': login,
            'password': password
        }
    );

    return {
        code: response.data.code,
        message: response.data.message
    }
}

const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(Login);

export default LoginContainer