import React from 'react';
import ReactDOM from 'react-dom';
import AppWrapper from './components/AppWrapper';

ReactDOM.render(
    <AppWrapper key="App" />,
  document.getElementById('root')
);
